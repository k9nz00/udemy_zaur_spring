package com.example.spring_learn.hibernate.hibernate_crud;

import com.example.spring_learn.hibernate.hibernate_crud.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Test2 {
    public static void main(String[] args) {

        //создание SessionFactory, сессии и сохранение объекта в бд
        try (SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory()) {

            Session session = factory.getCurrentSession();
            Employee employee1 = new Employee("Наташа", "Semka", "JavaScript", 1000);
            session.beginTransaction();
            session.save(employee1);

            int id = employee1.getId();

            Employee employee2 = session.find(Employee.class,  id);

            session.getTransaction().commit();
            System.out.println(employee2);
        }
    }
}
