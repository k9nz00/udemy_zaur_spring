package com.example.spring_learn.hibernate.hibernate_crud;

import com.example.spring_learn.hibernate.hibernate_crud.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class Test3 {
    public static void main(String[] args) {

        //пример выборки данных при помощи HQL
        try (SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory()) {

            Session session = factory.getCurrentSession();
            session.beginTransaction();

            List<Employee> employees = session.createQuery("from Employee")
                    .getResultList();

            employees.forEach(System.out::println);

            Query<Employee> query = session.createQuery("from Employee where name = :name");
            query.setParameter("name", "Andrey");
            List<Employee> resultList = query.getResultList();

            resultList.forEach(System.out::println);
            session.getTransaction().commit();
        }
    }
}
