package com.example.spring_learn.hibernate.hibernate_crud;

import com.example.spring_learn.hibernate.hibernate_crud.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Test1 {
    public static void main(String[] args) {

        //создание SessionFactory, сессии и сохранение объекта в бд
        try (SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory()) {

            Session session = factory.getCurrentSession();
            Employee employee1 = new Employee("Alena", "Lapa", "Java", 2500);
            session.beginTransaction();
            session.save(employee1);
            session.getTransaction().commit();
        }
    }
}
