
package com.example.spring_learn.hibernate.hibernate_crud;

import com.example.spring_learn.hibernate.hibernate_crud.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

public class Test4 {
    public static void main(String[] args) {

        try (SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory()) {

            Session session = factory.getCurrentSession();
            session.beginTransaction();

            //обновление одной сущности по primary key
            Employee employee = session.get(Employee.class, 1);
            employee.setSalary(51);

            //обновление группы сущностей
            Query<Integer> query = session.createQuery("update Employee set salary = :salary where surname =:surname");
            query.setParameter("salary", 8000);
            query.setParameter("surname", "Lapa");
            int countUpdatedRows = query.executeUpdate();

            System.out.println(countUpdatedRows);
            session.getTransaction().commit();
        }
    }
}
