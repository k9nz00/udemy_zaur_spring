
package com.example.spring_learn.hibernate.hibernate_crud;

import com.example.spring_learn.hibernate.hibernate_crud.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

public class Test5 {
    public static void main(String[] args) {

        try (SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory()) {

            Session session = factory.getCurrentSession();
            session.beginTransaction();

            //удаление одной сущности по primary key
            Employee employee = session.get(Employee.class, 1);
            session.delete(employee);

            //удаление группы сущностей
            Query<Integer> query = session.createQuery("delete Employee where id = :id");
            query.setParameter("id", 2);
            query.executeUpdate();

            session.getTransaction().commit();
        }
    }
}
