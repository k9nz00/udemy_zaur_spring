package com.example.spring_learn.hibernate.hibernate_relations.one_to_one.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "details")
@Data
@NoArgsConstructor
public class Detail {

    /*
        CREATE TABLE my_db.details (
          id int NOT NULL AUTO_INCREMENT,
          city varchar(15),
          phone_number varchar(25),
          email varchar(30), PRIMARY KEY (id)
        );
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "city")
    private String city;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    /*
    Налаживание двусторонней связи.
    Связь уже существует, нужно просто сказать куда смотреть.
    Для аттрибута 'mappedBy' нужно указать имя поля ('empDetail') в классе ('Employee'),  по которому уже была установлена односторонняя связь
     */

    @OneToOne(mappedBy = "empDetail", cascade = CascadeType.ALL)
    private Employee employee;

    public Detail(String city, String phoneNumber, String email) {
        this.city = city;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }
}
