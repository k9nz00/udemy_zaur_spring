package com.example.spring_learn.hibernate.hibernate_relations.one_to_one.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "employees")
@Data
@NoArgsConstructor
public class Employee {
/*
        CREATE TABLE my_db.employees (
        id int NOT NULL AUTO_INCREMENT,
        name varchar(15),
        surname varchar(25),
        department varchar(20), salary int, details_id int
    ,  PRIMARY KEY (id)
    , FOREIGN KEY (details_id) REFERENCES my_db.details(id));
 */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "department")
    private String department;

    @Column(name = "salary")
    private int salary;

    /**
    в объекте класса Employee будет поле empDetail
    при его помощи можно будет получить информацию от таблицы связанной с employees - с таблицы details
    Имя поля не обязано иметь такое же имя как и столбец для связи в таблице
    @JoinColumn(name = "details_id", referencedColumnName = "id")
        в аттрибуте 'name' указывается при помощи какого стоблца бд будет осуществляться связи.
        в аттрибуте 'table' - имя таблицу куда ссылаемся (необязательный параметр).
        в аттрибуте 'referencedColumnName' - на какой столбец присоединяемой таблицы будет ссылаться.
     */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "details_id")
    private Detail empDetail;

    public Employee(String name, String surname, String department, int salary) {
        this.name = name;
        this.surname = surname;
        this.department = department;
        this.salary = salary;
    }
}
