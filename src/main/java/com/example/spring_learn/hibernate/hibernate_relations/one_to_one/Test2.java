package com.example.spring_learn.hibernate.hibernate_relations.one_to_one;

import com.example.spring_learn.hibernate.hibernate_relations.one_to_one.entity.Detail;
import com.example.spring_learn.hibernate.hibernate_relations.one_to_one.entity.Employee;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Test2 {

    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {

        //создание двусторонней связи типа one-to-one
        try (SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .addAnnotatedClass(Detail.class)
                .buildSessionFactory();
             Session session = factory.getCurrentSession()) {

            Employee employee = new Employee("Наташа", "Semka", "IT", 200);
            Detail detail = new Detail("Лазурный", "+7987456123", "email1@email.com");
            session.beginTransaction();

            //необходимо установить и работка в детали и детали дл работника
            detail.setEmployee(employee);
            employee.setEmpDetail(detail);
            session.save(detail);
            session.getTransaction().commit();
        }
    }
}
