package com.example.spring_learn.hibernate.hibernate_relations.many_to_one.uni_directional;

import com.example.spring_learn.hibernate.hibernate_relations.many_to_one.uni_directional.entity.Department;
import com.example.spring_learn.hibernate.hibernate_relations.many_to_one.uni_directional.entity.Employee;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Test1 {

    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {

        //пример работы и использования many-to-one
        try (SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .addAnnotatedClass(Department.class)
                .buildSessionFactory();
             Session session = factory.getCurrentSession()) {

            Department department = new Department("HR", 500, 2000);
            Employee emp1 = new Employee("User1", "Test", 500);
            Employee emp2 = new Employee("User2", "Test", 800);
            department.addEmployeeToDepartment(emp1);
            department.addEmployeeToDepartment(emp2);
            session.beginTransaction();
            session.save(department);
            session.getTransaction().commit();

//            session.beginTransaction();
//            Department department = session.get(Department.class, 1);
//            List<Employee> employees = department.getEmployees();
//            employees.forEach(System.out::println);
//            session.getTransaction().commit();
//
//            session.beginTransaction();
//            Employee employee = session.get(Employee.class, 1);
//            Department department = employee.getDepartment();
//            System.out.println(department);
//            session.getTransaction().commit();
        }
    }
}
