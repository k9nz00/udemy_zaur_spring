package com.example.spring_learn.hibernate.hibernate_relations.one_to_one;

import com.example.spring_learn.hibernate.hibernate_relations.one_to_one.entity.Detail;
import com.example.spring_learn.hibernate.hibernate_relations.one_to_one.entity.Employee;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Test1 {

    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {

        //создание односторонней связи типа one-to-one
        try (SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .addAnnotatedClass(Detail.class)
                .buildSessionFactory()) {
            Session session = factory.getCurrentSession();

            Employee employee = new Employee("Andrey", "Semka", "IT", 100);
            Detail detail = new Detail("Krasnodar", "+7123456789", "email@email.com");
            employee.setEmpDetail(detail);
            session.beginTransaction();
            session.save(employee);

            session.getTransaction().commit();

            logger.info(employee);
        }
    }
}
