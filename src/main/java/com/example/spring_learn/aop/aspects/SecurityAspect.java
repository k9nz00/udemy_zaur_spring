package com.example.spring_learn.aop.aspects;

import com.example.spring_learn.aop.UniLibrary;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@Order(100)
@Aspect
public class SecurityAspect {

    @Before("com.example.spring_learn.aop.aspects.MyPointcuts.allAddMethods()")
    public void beforeAddSecurityAdvice(JoinPoint joinPoint) {

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Object[] args = joinPoint.getArgs();

        //имя метода
        System.out.println("signature.getName() " + signature.getName());

        //возвращаемый тип данных
        System.out.println("signature.getReturnType() " + signature.getReturnType());

        //
        System.out.println("signature.getMethod() " + signature.getMethod());
        System.out.println("signature " + signature);


        if (args.length > 0) {
            Arrays.stream(args)
                    .forEach(System.out::println);
        }

        System.out.println("beforeAddSecurityAdvice: проверка доступа при обращении с " + UniLibrary.class.getSimpleName());
        System.out.println("----------------------------------------------");
    }
}
