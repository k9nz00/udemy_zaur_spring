package com.example.spring_learn.aop.aspects;

import com.example.spring_learn.aop.UniLibrary;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Order(500)
public class ExceptionHandingAspect {

    @Before("com.example.spring_learn.aop.aspects.MyPointcuts.allAddMethods()")
    public void beforeAddExceptionHandingAdvice() {
        System.out.println("beforeAddExceptionHandingAdvice: ловим/обрабатываем исключения при попытке получить книгу/журнал " + UniLibrary.class.getSimpleName());
        System.out.println("----------------------------------------------");
    }
}
