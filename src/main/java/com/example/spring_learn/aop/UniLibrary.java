package com.example.spring_learn.aop;

import org.springframework.stereotype.Component;

@Component
public class UniLibrary{

    public void getBook(){
        System.out.println("Мы берем книгу из университетской библиотеки: " + this.getClass().getSimpleName());
        System.out.println("----------------------------------------------");
    }

    public void returnBook(){
        System.out.println("Мы возвращаем книгу университетскую библиотеку: " + this.getClass().getSimpleName());
        System.out.println("----------------------------------------------");
    }

    public void getMagazine(){
        System.out.println("Мы берем журнал из университетской библиотеки: " + this.getClass().getSimpleName());
        System.out.println("----------------------------------------------");
    }

    public void returnMagazine(){
        System.out.println("Мы возвращаем журнал университетскую библиотеку: " + this.getClass().getSimpleName());
        System.out.println("----------------------------------------------");
    }

    public void addBook(String personName, Book book){
        System.out.println("Мы добавляем книгу в университетскую библиотеку: " + this.getClass().getSimpleName());
        System.out.println("----------------------------------------------");
    }

    public void addMagazine(){
        System.out.println("Мы добавляем журнал в университетскую библиотеку: " + this.getClass().getSimpleName());
        System.out.println("----------------------------------------------");
    }
}
