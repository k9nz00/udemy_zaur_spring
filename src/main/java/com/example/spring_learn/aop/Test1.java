package com.example.spring_learn.aop;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test1 {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MyConfig.class);

        UniLibrary uniLibrary = context.getBean("uniLibrary", UniLibrary.class);
        uniLibrary.getBook();
        String person = "Вася";
        Book book = context.getBean("book", Book.class);
        uniLibrary.addBook(person, book);
        uniLibrary.getMagazine();

        context.close();
    }
}
