package com.example.spring_learn.aop;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class Book {

    @Value("Преступление и наказание")
    private String name;

    @Value("Достоевский")
    private String author;

    @Value("1866")
    private int yearOfPublication;
}
