package com.example.spring_learn.aop;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan("com.example.spring_learn.aop")
@EnableAspectJAutoProxy
public class MyConfig {
}
