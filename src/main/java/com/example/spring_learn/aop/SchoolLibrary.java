package com.example.spring_learn.aop;

import org.springframework.stereotype.Component;

@Component
public class SchoolLibrary {

//    @Override
    public void getBook() {
        System.out.println("мы берем книгу из школьной библиотеки");
    }

    public void returnBook(){
        System.out.println("мы возвращаем книгу в школьную библиотеку");
    }
}
