package com.example.spring_learn.introduction;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ConfigWithAnnotation1 {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext2.xml");

        Person person = context.getBean("person", Person.class);
        person.callPet();

        System.out.println(org.springframework.core.SpringVersion.getVersion());

        context.close();
    }
}
