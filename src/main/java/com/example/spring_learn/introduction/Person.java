package com.example.spring_learn.introduction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
//@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Person {
    private final Pet pet;

    @Autowired
    public Person(@Qualifier("cat") Pet pet) {
        this.pet = pet;
    }

    public void callPet() {
        System.out.println("привет мой питомец!");
        pet.say();
    }
}
